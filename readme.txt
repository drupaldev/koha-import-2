
Koha data import with vufind harvester

- Copy and extract vufindharvest-master folder to drupal folder 
- Go to vufindharvest-master folder
- Add libraries to vufindharvest-master/etc/oai.ini file 
- Run command `php bin/harvest_oai.php --ini=etc/oai.ini nvlicsl` from vufindharvest-master directory. 
  This will create directory in vufindharvest-master directory/data folder [`nvlicsl` in above case] and harvest related data.
- Copy and extract koha-import folder to drupal modules directory
- Change file owner permission to www-data:www-data
    $sudo chown www-data:www-data kohaimport
- Install module kohaimport
- Run cron job from drupal interface